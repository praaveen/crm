class NotificationsMailer < ActionMailer::Base
  default :from => "gmail.com"
  default :to => "praaveen.ece@gmail.com"

  def new_contact(contact)
    @contact = contact
    mail(:subject => "[contact form] #{contact.subject}")
  end
end
