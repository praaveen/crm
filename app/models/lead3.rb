class Lead3 < ActiveRecord::Base
  attr_accessible :address, :appointment_date, :appointment_time, :city, :company_name, :email, :first_name, :last_name, :message, :phone, :postal_code, :state
end
