class Lead2sController < ApplicationController
  # GET /lead2s
  # GET /lead2s.json
  def index
    @lead2s = Lead2.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lead2s }
    end
  end

  # GET /lead2s/1
  # GET /lead2s/1.json
  def show
    @lead2 = Lead2.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lead2 }
    end
  end

  # GET /lead2s/new
  # GET /lead2s/new.json
  def new
    @lead2 = Lead2.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lead2 }
    end
  end

  # GET /lead2s/1/edit
  def edit
    @lead2 = Lead2.find(params[:id])
  end

  # POST /lead2s
  # POST /lead2s.json
  def create
    @lead2 = Lead2.new(params[:lead2])

    respond_to do |format|
      if @lead2.save
        format.html { redirect_to @lead2, notice: 'Lead2 was successfully created.' }
        format.json { render json: @lead2, status: :created, location: @lead2 }
      else
        format.html { render action: "new" }
        format.json { render json: @lead2.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lead2s/1
  # PUT /lead2s/1.json
  def update
    @lead2 = Lead2.find(params[:id])

    respond_to do |format|
      if @lead2.update_attributes(params[:lead2])
        format.html { redirect_to @lead2, notice: 'Lead2 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lead2.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lead2s/1
  # DELETE /lead2s/1.json
  def destroy
    @lead2 = Lead2.find(params[:id])
    @lead2.destroy

    respond_to do |format|
      format.html { redirect_to lead2s_url }
      format.json { head :no_content }
    end
  end
end
