class Lead5sController < ApplicationController
  # GET /lead5s
  # GET /lead5s.json
  def index
    @lead5s = Lead5.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lead5s }
    end
  end

  # GET /lead5s/1
  # GET /lead5s/1.json
  def show
    @lead5 = Lead5.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lead5 }
    end
  end

  # GET /lead5s/new
  # GET /lead5s/new.json
  def new
    @lead5 = Lead5.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lead5 }
    end
  end

  # GET /lead5s/1/edit
  def edit
    @lead5 = Lead5.find(params[:id])
  end

  # POST /lead5s
  # POST /lead5s.json
  def create
    @lead5 = Lead5.new(params[:lead5])

    respond_to do |format|
      if @lead5.save
        format.html { redirect_to @lead5, notice: 'Lead5 was successfully created.' }
        format.json { render json: @lead5, status: :created, location: @lead5 }
      else
        format.html { render action: "new" }
        format.json { render json: @lead5.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lead5s/1
  # PUT /lead5s/1.json
  def update
    @lead5 = Lead5.find(params[:id])

    respond_to do |format|
      if @lead5.update_attributes(params[:lead5])
        format.html { redirect_to @lead5, notice: 'Lead5 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lead5.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lead5s/1
  # DELETE /lead5s/1.json
  def destroy
    @lead5 = Lead5.find(params[:id])
    @lead5.destroy

    respond_to do |format|
      format.html { redirect_to lead5s_url }
      format.json { head :no_content }
    end
  end
end
