class Lead4sController < ApplicationController
  # GET /lead4s
  # GET /lead4s.json
  def index
    @lead4s = Lead4.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lead4s }
    end
  end

  # GET /lead4s/1
  # GET /lead4s/1.json
  def show
    @lead4 = Lead4.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lead4 }
    end
  end

  # GET /lead4s/new
  # GET /lead4s/new.json
  def new
    @lead4 = Lead4.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lead4 }
    end
  end

  # GET /lead4s/1/edit
  def edit
    @lead4 = Lead4.find(params[:id])
  end

  # POST /lead4s
  # POST /lead4s.json
  def create
    @lead4 = Lead4.new(params[:lead4])

    respond_to do |format|
      if @lead4.save
        format.html { redirect_to @lead4, notice: 'Lead4 was successfully created.' }
        format.json { render json: @lead4, status: :created, location: @lead4 }
      else
        format.html { render action: "new" }
        format.json { render json: @lead4.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lead4s/1
  # PUT /lead4s/1.json
  def update
    @lead4 = Lead4.find(params[:id])

    respond_to do |format|
      if @lead4.update_attributes(params[:lead4])
        format.html { redirect_to @lead4, notice: 'Lead4 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lead4.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lead4s/1
  # DELETE /lead4s/1.json
  def destroy
    @lead4 = Lead4.find(params[:id])
    @lead4.destroy

    respond_to do |format|
      format.html { redirect_to lead4s_url }
      format.json { head :no_content }
    end
  end
end
