class Lead1sController < ApplicationController
  # GET /lead1s
  # GET /lead1s.json
  def index
    @lead1s = Lead1.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lead1s }
    end
  end

  # GET /lead1s/1
  # GET /lead1s/1.json
  def show
    @lead1 = Lead1.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lead1 }
    end
  end

  # GET /lead1s/new
  # GET /lead1s/new.json
  def new
    @lead1 = Lead1.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lead1 }
    end
  end

  # GET /lead1s/1/edit
  def edit
    @lead1 = Lead1.find(params[:id])
  end

  # POST /lead1s
  # POST /lead1s.json
  def create
    @lead1 = Lead1.new(params[:lead1])

    respond_to do |format|
      if @lead1.save
        format.html { redirect_to @lead1, notice: 'Lead1 was successfully created.' }
        format.json { render json: @lead1, status: :created, location: @lead1 }
      else
        format.html { render action: "new" }
        format.json { render json: @lead1.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lead1s/1
  # PUT /lead1s/1.json
  def update
    @lead1 = Lead1.find(params[:id])

    respond_to do |format|
      if @lead1.update_attributes(params[:lead1])
        format.html { redirect_to @lead1, notice: 'Lead1 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lead1.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lead1s/1
  # DELETE /lead1s/1.json
  def destroy
    @lead1 = Lead1.find(params[:id])
    @lead1.destroy

    respond_to do |format|
      format.html { redirect_to lead1s_url }
      format.json { head :no_content }
    end
  end
end
