class Lead3sController < ApplicationController
  # GET /lead3s
  # GET /lead3s.json
  def index
    @lead3s = Lead3.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lead3s }
    end
  end

  # GET /lead3s/1
  # GET /lead3s/1.json
  def show
    @lead3 = Lead3.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lead3 }
    end
  end

  # GET /lead3s/new
  # GET /lead3s/new.json
  def new
    @lead3 = Lead3.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lead3 }
    end
  end

  # GET /lead3s/1/edit
  def edit
    @lead3 = Lead3.find(params[:id])
  end

  # POST /lead3s
  # POST /lead3s.json
  def create
    @lead3 = Lead3.new(params[:lead3])

    respond_to do |format|
      if @lead3.save
        format.html { redirect_to @lead3, notice: 'Lead3 was successfully created.' }
        format.json { render json: @lead3, status: :created, location: @lead3 }
      else
        format.html { render action: "new" }
        format.json { render json: @lead3.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lead3s/1
  # PUT /lead3s/1.json
  def update
    @lead3 = Lead3.find(params[:id])

    respond_to do |format|
      if @lead3.update_attributes(params[:lead3])
        format.html { redirect_to @lead3, notice: 'Lead3 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lead3.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lead3s/1
  # DELETE /lead3s/1.json
  def destroy
    @lead3 = Lead3.find(params[:id])
    @lead3.destroy

    respond_to do |format|
      format.html { redirect_to lead3s_url }
      format.json { head :no_content }
    end
  end
end
