require 'test_helper'

class Lead2sControllerTest < ActionController::TestCase
  setup do
    @lead2 = lead2s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lead2s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lead2" do
    assert_difference('Lead2.count') do
      post :create, lead2: { address: @lead2.address, appointment_date: @lead2.appointment_date, appointment_time: @lead2.appointment_time, city: @lead2.city, company_name: @lead2.company_name, email: @lead2.email, first_name: @lead2.first_name, last_name: @lead2.last_name, message: @lead2.message, phone: @lead2.phone, postal_code: @lead2.postal_code, state: @lead2.state }
    end

    assert_redirected_to lead2_path(assigns(:lead2))
  end

  test "should show lead2" do
    get :show, id: @lead2
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lead2
    assert_response :success
  end

  test "should update lead2" do
    put :update, id: @lead2, lead2: { address: @lead2.address, appointment_date: @lead2.appointment_date, appointment_time: @lead2.appointment_time, city: @lead2.city, company_name: @lead2.company_name, email: @lead2.email, first_name: @lead2.first_name, last_name: @lead2.last_name, message: @lead2.message, phone: @lead2.phone, postal_code: @lead2.postal_code, state: @lead2.state }
    assert_redirected_to lead2_path(assigns(:lead2))
  end

  test "should destroy lead2" do
    assert_difference('Lead2.count', -1) do
      delete :destroy, id: @lead2
    end

    assert_redirected_to lead2s_path
  end
end
