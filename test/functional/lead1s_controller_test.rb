require 'test_helper'

class Lead1sControllerTest < ActionController::TestCase
  setup do
    @lead1 = lead1s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lead1s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lead1" do
    assert_difference('Lead1.count') do
      post :create, lead1: { address: @lead1.address, appointment_date: @lead1.appointment_date, appointment_time: @lead1.appointment_time, city: @lead1.city, company_name: @lead1.company_name, email: @lead1.email, first_name: @lead1.first_name, last_name: @lead1.last_name, message: @lead1.message, phone: @lead1.phone, postal_code: @lead1.postal_code, state: @lead1.state }
    end

    assert_redirected_to lead1_path(assigns(:lead1))
  end

  test "should show lead1" do
    get :show, id: @lead1
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lead1
    assert_response :success
  end

  test "should update lead1" do
    put :update, id: @lead1, lead1: { address: @lead1.address, appointment_date: @lead1.appointment_date, appointment_time: @lead1.appointment_time, city: @lead1.city, company_name: @lead1.company_name, email: @lead1.email, first_name: @lead1.first_name, last_name: @lead1.last_name, message: @lead1.message, phone: @lead1.phone, postal_code: @lead1.postal_code, state: @lead1.state }
    assert_redirected_to lead1_path(assigns(:lead1))
  end

  test "should destroy lead1" do
    assert_difference('Lead1.count', -1) do
      delete :destroy, id: @lead1
    end

    assert_redirected_to lead1s_path
  end
end
