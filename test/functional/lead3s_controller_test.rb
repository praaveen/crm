require 'test_helper'

class Lead3sControllerTest < ActionController::TestCase
  setup do
    @lead3 = lead3s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lead3s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lead3" do
    assert_difference('Lead3.count') do
      post :create, lead3: { address: @lead3.address, appointment_date: @lead3.appointment_date, appointment_time: @lead3.appointment_time, city: @lead3.city, company_name: @lead3.company_name, email: @lead3.email, first_name: @lead3.first_name, last_name: @lead3.last_name, message: @lead3.message, phone: @lead3.phone, postal_code: @lead3.postal_code, state: @lead3.state }
    end

    assert_redirected_to lead3_path(assigns(:lead3))
  end

  test "should show lead3" do
    get :show, id: @lead3
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lead3
    assert_response :success
  end

  test "should update lead3" do
    put :update, id: @lead3, lead3: { address: @lead3.address, appointment_date: @lead3.appointment_date, appointment_time: @lead3.appointment_time, city: @lead3.city, company_name: @lead3.company_name, email: @lead3.email, first_name: @lead3.first_name, last_name: @lead3.last_name, message: @lead3.message, phone: @lead3.phone, postal_code: @lead3.postal_code, state: @lead3.state }
    assert_redirected_to lead3_path(assigns(:lead3))
  end

  test "should destroy lead3" do
    assert_difference('Lead3.count', -1) do
      delete :destroy, id: @lead3
    end

    assert_redirected_to lead3s_path
  end
end
