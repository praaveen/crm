require 'test_helper'

class Lead5sControllerTest < ActionController::TestCase
  setup do
    @lead5 = lead5s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lead5s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lead5" do
    assert_difference('Lead5.count') do
      post :create, lead5: { address: @lead5.address, appointment_date: @lead5.appointment_date, appointment_time: @lead5.appointment_time, city: @lead5.city, company_name: @lead5.company_name, email: @lead5.email, first_name: @lead5.first_name, last_name: @lead5.last_name, message: @lead5.message, phone: @lead5.phone, postal_code: @lead5.postal_code, state: @lead5.state }
    end

    assert_redirected_to lead5_path(assigns(:lead5))
  end

  test "should show lead5" do
    get :show, id: @lead5
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lead5
    assert_response :success
  end

  test "should update lead5" do
    put :update, id: @lead5, lead5: { address: @lead5.address, appointment_date: @lead5.appointment_date, appointment_time: @lead5.appointment_time, city: @lead5.city, company_name: @lead5.company_name, email: @lead5.email, first_name: @lead5.first_name, last_name: @lead5.last_name, message: @lead5.message, phone: @lead5.phone, postal_code: @lead5.postal_code, state: @lead5.state }
    assert_redirected_to lead5_path(assigns(:lead5))
  end

  test "should destroy lead5" do
    assert_difference('Lead5.count', -1) do
      delete :destroy, id: @lead5
    end

    assert_redirected_to lead5s_path
  end
end
