require 'test_helper'

class Lead4sControllerTest < ActionController::TestCase
  setup do
    @lead4 = lead4s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lead4s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lead4" do
    assert_difference('Lead4.count') do
      post :create, lead4: { address: @lead4.address, appointment_date: @lead4.appointment_date, appointment_time: @lead4.appointment_time, city: @lead4.city, company_name: @lead4.company_name, email: @lead4.email, first_name: @lead4.first_name, last_name: @lead4.last_name, message: @lead4.message, phone: @lead4.phone, postal_code: @lead4.postal_code, state: @lead4.state }
    end

    assert_redirected_to lead4_path(assigns(:lead4))
  end

  test "should show lead4" do
    get :show, id: @lead4
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lead4
    assert_response :success
  end

  test "should update lead4" do
    put :update, id: @lead4, lead4: { address: @lead4.address, appointment_date: @lead4.appointment_date, appointment_time: @lead4.appointment_time, city: @lead4.city, company_name: @lead4.company_name, email: @lead4.email, first_name: @lead4.first_name, last_name: @lead4.last_name, message: @lead4.message, phone: @lead4.phone, postal_code: @lead4.postal_code, state: @lead4.state }
    assert_redirected_to lead4_path(assigns(:lead4))
  end

  test "should destroy lead4" do
    assert_difference('Lead4.count', -1) do
      delete :destroy, id: @lead4
    end

    assert_redirected_to lead4s_path
  end
end
