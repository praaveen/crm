class CreateLead4s < ActiveRecord::Migration
  def change
    create_table :lead4s do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :phone
      t.string :message
      t.date :appointment_date
      t.string :appointment_time
      t.string :address
      t.string :city
      t.string :state
      t.integer :postal_code
      t.string :company_name

      t.timestamps
    end
  end
end
